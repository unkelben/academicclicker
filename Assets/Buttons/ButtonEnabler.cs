using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEnabler : MonoBehaviour
{
    
    AcademicManager AM;
    public int cost = 100; //data needed to be available
    public string id = ""; //a recognizable name so the manager can set its cost
    public bool researcher = false;

    // Start is called before the first frame update
    void Start()
    {
        AM = GameObject.FindObjectOfType<AcademicManager>();   
    }

    // Update is called once per frame
    void Update()
    {
        int Resource;
        if (researcher)
            Resource = AM.Funding;
        else
            Resource = AM.Data;

        if (Resource >= cost)
        {
            GetComponent<Button>().interactable = true;
        }
        else
        {
            GetComponent<Button>().interactable = false;
        }
    }

    public void UpdateLabel()
    {
        Text text = GetComponentInChildren<Text>();
        text.text += "\n(" + cost + ")";
    }
}
