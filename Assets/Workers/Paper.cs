using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paper : AcademicWorker
{
    int CitationOutput = 0;
    int Citations = 0;

    public Paper(AcademicManager _AM, int _output)
    {
        AM = _AM;
        CitationOutput = _output;
    }

    public override void DoOneMonth()
    {
        AM.AddCitation(CitationOutput);
        Citations += CitationOutput;
    }

}
