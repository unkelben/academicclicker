using System.Collections;
using System.Collections.Generic;

public class Researcher : AcademicWorker
{
    int ResearchOutput = 0;

    public Researcher(AcademicManager _AM, int _output)
    {
        AM = _AM;
        ResearchOutput = _output;
    }

    public override void DoOneMonth()
    {
        AM.AddData(ResearchOutput);
    }

}
