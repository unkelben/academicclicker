using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AcademicManager : MonoBehaviour
{
    [Header("Current Game Variables")]
    public int CurrentMonth = 1;
    public int CurrentAge = 30;
    public string ProfName = "Albus Dumbledore";
    public int Data = 0;
    public int Citations = 0;
    public int Funding = 0;

    [Header("Parameters")]
    public float SecondsPerMonth = 3; //How many real seconds does it take for a month to pass?
    public int StartingAge = 30;

    [Header("Publications")]
    public int ConferencePaperCost = 100;
    public int ConferencePaperOutput = 2;
    public int BookChapterCost = 200;
    public int BookChapterOutput = 3;
    public int JournalArticleCost = 200;
    public int JournalArticleOutput = 6;
    public int BookCost = 1000;
    public int BookOutput = 20;

    [Header("Grants")]
    public int SmallGrantCost = 100;
    public int SmallGrantOutput = 50;
    public int MediumGrantCost = 200;
    public int MediumGrantOutput = 150;
    public int BigGrantCost = 300;
    public int BigGrantOutput = 500;

    [Header("Researchers")]
    public int MasterStudentCost = 40;
    public int MasterStudentOutput = 1;
    public int PhdStudentCost = 100;
    public int PhdStudentOutput = 2;
    public int PostdocCost = 120;
    public int PostdocOutput = 10;

    //References
    GUIManager GM;

    //Workers
    List<AcademicWorker> AcademicWorkers = new List<AcademicWorker>();

    // Start is called before the first frame update
    void Start()
    {
        GM = GetComponent<GUIManager>();
        CurrentAge = StartingAge;        
        StartCoroutine(TimeManager());
    }

    // Timer
    IEnumerator TimeManager()
    {
        while(true == true)
        {            
            yield return new WaitForSeconds(SecondsPerMonth);

            //things that happen every month
            CurrentMonth++;
            if (CurrentMonth > 11)
            {
                CurrentMonth = 0;
                CurrentAge++;
            }
            GM.UpdateProf();
            GM.UpdateCitations();
            GM.UpdateData();
            GM.UpdateFunding();

            //go through the list of workers and get them to do their thing
            foreach (AcademicWorker worker in AcademicWorkers)
            {
                worker.DoOneMonth();
            }

        }    
    }

    //Interacting with the variables
    public void AddData(int amount)
    {
        Data += amount;
        GM.UpdateData();
        //perhaps some visual pizzazz here
    }
    public void AddCitation(int amount)
    {
        Citations += amount;
        GM.UpdateCitations();
        //perhaps some visual pizzazz here
    }
    public void AddFunding(int amount)
    {
        Funding += amount;
        GM.UpdateFunding();
        //perhaps some visual pizzazz here
    } 

    //Buying workers
    public void PublishConferencePaper()
    {
        if (Data >= ConferencePaperCost)
        {
            Data -= ConferencePaperCost;
            Paper paper = new Paper(this, ConferencePaperOutput);
            paper.name = "Title. Conference paper.";
            AcademicWorkers.Add(paper);
            GM.AddPaper(paper);
        }
    }
    public void PublishBookChapter()
    {
        if (Data >= BookChapterCost)
        {
            Data -= BookChapterCost;
            Paper paper = new Paper(this, BookChapterOutput);
            paper.name = "Title. Book chapter.";
            AcademicWorkers.Add(paper);
            GM.AddPaper(paper);
        }
    }
    public void PublishJournalArticle()
    {
        if (Data >= JournalArticleCost)
        {
            Data -= JournalArticleCost;
            Paper paper = new Paper(this, JournalArticleOutput);
            AcademicWorkers.Add(paper);
            paper.name = "Title. Journal article.";
            GM.AddPaper(paper);
        }
    }
    public void PublishBook()
    {
        if (Data >= BookCost)
        {
            Data -= BookCost;
            Paper paper = new Paper(this, BookOutput);
            AcademicWorkers.Add(paper);
            paper.name = "Title. Book.";
            GM.AddPaper(paper);
        }
    }

    public void WriteSmallGrant()
    {
        if (Data >= SmallGrantCost)
        {
            Data -= SmallGrantCost;
            AddFunding(SmallGrantOutput);
        }
    }

    public void WriteMediumGrant()
    {
        if (Data >= MediumGrantCost)
        {
            Data -= MediumGrantCost;
            AddFunding(MediumGrantOutput);
        }
    }
    public void WriteBigGrant()
    {
        if (Data >= BigGrantCost)
        {
            Data -= BigGrantCost;
            AddFunding(BigGrantOutput);
            AddFunding(BigGrantOutput);
        }
    }
    public void HireMasterStudent()
    {
        if (Funding >= MasterStudentCost)
        {
            Funding -= MasterStudentCost;
            Researcher researcher = new Researcher(this, MasterStudentOutput);
            AcademicWorkers.Add(researcher);
            researcher.name = "John Smith. Master Student.";
            GM.AddStudent(researcher);
        }
    }
    public void HirePhdStudent()
    {
        if (Funding >= PhdStudentCost)
        {
            Funding -= PhdStudentCost;
            Researcher researcher = new Researcher(this, PhdStudentOutput);
            AcademicWorkers.Add(researcher);
            researcher.name = "John Smith. Phd Student.";
            GM.AddStudent(researcher);
        }
    }
    public void HirePostdoc()
    {
        if (Funding >= PostdocCost)
        {
            Funding -= PostdocCost;
            Researcher researcher = new Researcher(this, PostdocOutput);
            AcademicWorkers.Add(researcher);
            researcher.name = "John Smith. Postdoctoral fellow.";
            GM.AddStudent(researcher);
        }
    }
}
