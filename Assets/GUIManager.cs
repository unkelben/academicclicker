using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIManager : MonoBehaviour
{
    [Header("References")]
    public TMPro.TextMeshProUGUI ProfLabel;
    public TMPro.TextMeshProUGUI DataLabel;
    public TMPro.TextMeshProUGUI CitationLabel;
    public TMPro.TextMeshProUGUI Bibliography;
    public TMPro.TextMeshProUGUI FundingLabel;
    public TMPro.TextMeshProUGUI StudentList;

    AcademicManager AM;

    // Start is called before the first frame update
    void Start()
    {
        AM = GetComponent<AcademicManager>();
        SetCosts(); //Update all button costs with master values
    }


    //Clicks!
    public void ResearchClick()
    {
        AM.AddData(1);
        UpdateData();
        GameObject.FindObjectOfType<Gauge>().ChangePercent(1);
    }

    //UI stuff (could be moved to another class)
    public void UpdateData()
    {
        DataLabel.text = "Available Data: " + AM.Data;
    }
    public void UpdateProf()
    {
        ProfLabel.text = AM.ProfName + ", " + AM.CurrentAge + " years and " + AM.CurrentMonth + " months";
    }
    public void AddPaper(AcademicWorker paper)
    {
        Bibliography.text += paper.name + "\n";
    }
    public void AddStudent(AcademicWorker researcher)
    {
        StudentList.text += researcher.name + "\n";
    }
    public void UpdateCitations()
    {
        CitationLabel.text = AM.Citations + " citations (" + ((float)AM.Citations / 1238354f) + "% of Foucault)";
    }
    public void UpdateFunding()
    {
        FundingLabel.text = "Available funds: " + AM.Funding + "K";
    }

    //this function crawls through all buttons and sets the right costs for them to
    //be enabled at the right time
    //(this is terrible)
    public void SetCosts()
    {
        ButtonEnabler[] allbuttons = GameObject.FindObjectsOfType<ButtonEnabler>();
        foreach (ButtonEnabler thing in allbuttons)
        {
            switch (thing.id)
            {
                case "ConferencePaper":
                    thing.cost = AM.ConferencePaperCost;
                    thing.UpdateLabel();
                    break;

                case "BookChapter":
                    thing.cost = AM.BookChapterCost;
                    thing.UpdateLabel();
                    break;

                case "JournalArticle":
                    thing.cost = AM.JournalArticleCost;
                    thing.UpdateLabel();
                    break;

                case "Book":
                    thing.cost = AM.BookCost;
                    thing.UpdateLabel();
                    break;

                case "SmallGrant":
                    thing.cost = AM.SmallGrantCost;
                    thing.UpdateLabel();
                    break;

                case "MediumGrant":
                    thing.cost = AM.MediumGrantCost;
                    thing.UpdateLabel();
                    break;

                case "BigGrant":
                    thing.cost = AM.BigGrantCost;
                    thing.UpdateLabel();
                    break;

                case "MasterStudent":
                    thing.cost = AM.MasterStudentCost;
                    thing.UpdateLabel();
                    break;

                case "PhdStudent":
                    thing.cost = AM.PhdStudentCost;
                    thing.UpdateLabel();
                    break;

                case "Postdoc":
                    thing.cost = AM.PostdocCost;
                    thing.UpdateLabel();
                    break;

                default:
                    thing.cost = 1000;
                    thing.UpdateLabel();
                    break;
            }
        }
    }
}
