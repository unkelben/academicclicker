using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gauge : MonoBehaviour
{

    public float Fullsize = 100;
    public float PercentFilled = 100;
    public Image Filler;
    float FillerHeight = 10;

    void Start()
    {
        FillerHeight = Filler.GetComponent<RectTransform>().rect.height;
        ChangePercent(0);
    }

    public void ChangePercent(float percent)
    {
        PercentFilled += percent;
        float FillerSize = (Fullsize * PercentFilled) / 100;
        Filler.GetComponent<RectTransform>().sizeDelta = new Vector2(FillerSize, FillerHeight);
    }
    

}
